set log
# Survival probability
#plot 'geom_ne_I1e6_T500_r0.0_D0.1_u0.1.out',\
 'geom_ne_I1e6_T500_r0.0_D0.1_u0.2.out', \
'geom_ne_I1e6_T500_r0.0_D0.1_u0.4.out',\
 'geom_ne_I1e6_T500_r0.0_D0.1_u0.5.out', \
'geom_ne_I1e6_T500_r0.0_D0.1_u0.6.out',\
'geom_ne_I1e6_T500_r0.0_D0.1_u0.7.out',\
 'geom_ne_I1e6_T500_r0.0_D0.1_u0.8.out'

# First moment
plot 'geom_ne_I1e6_T500_r0.0_D0.1_u0.1.out' u 1:3,\
 'geom_ne_I1e6_T500_r0.0_D0.1_u0.2.out' u 1:3, \
'geom_ne_I1e6_T500_r0.0_D0.1_u0.4.out' u 1:3,\
 'geom_ne_I1e6_T500_r0.0_D0.1_u0.5.out' u 1:3, \
'geom_ne_I1e6_T500_r0.0_D0.1_u0.6.out' u 1:3,\
'geom_ne_I1e6_T500_r0.0_D0.1_u0.7.out' u 1:3,\
 'geom_ne_I1e6_T500_r0.0_D0.1_u0.8.out' u 1:3
