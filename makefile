# makefile for repelling_line.c

CC = gcc
LDFLAGS = -lm -lgsl -lgslcblas 
OPTIM = -O3
CFLAGS += -Wall -Wextra -Wpedantic \
	            -Wformat=2 -Wno-unused-parameter -Wshadow \
		              -Wwrite-strings -Wstrict-prototypes -Wold-style-definition \
			                -Wredundant-decls -Wnested-externs -Wmissing-include-dirs -std=c11 #-fsanitize=address
ALL_SRCS=$(wildcard *.c)
SRCS = $(filter-out mt199337.c, $(ALL_SRCS))
PROGS = $(patsubst %.c,%,$(SRCS))

all: $(PROGS)

%: %.c

	$(CC) $(CFLAGS) $(LDFLAGS) $(OPTIM) -o $@ $<
