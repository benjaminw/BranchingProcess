#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>
// Macros
#define RANDOM_DOUBLE gsl_ran_flat(r,0.0,1.0);
#define EXP_WAIT(n) gsl_ran_exponential(r, 1/((double) n))
#define TREEMEMSIZE 100
#define ADDTREEMEMSIZE 100
#define RESCALINGRES 1000
#define EPS 0.00000001
#define TIME_MAX 10000

// Structures
struct branching_process // Structure to describe a relisation of a branching process 
{
	double *event_times; // records times at which any event was triggered
	int *population_size; // records population size
	double lifetime; // records lifetime of branching_tree
	int event_counts; // counts how many events have ocurred.
	int array_max;
};

struct rescaled_profile // Structure to rescribe rescaled process and its weigh
{
	int *profile;
	double original_lifetime;
	double weight;
};

struct averaged_profile
{
	double *profile;
	double sum_of_weights;
};

// Functions
void initialise(struct branching_process** init_tree, struct rescaled_profile** rs_tree, struct averaged_profile** avg_profile);
void generate_tree(struct branching_process** tree, double pbranch);
void grow(struct branching_process** tree);
int offspring(double pbranch);
void rescale_tree(struct branching_process* original_tree, struct rescaled_profile** rs_tree);
void add_to_average(struct rescaled_profile* resized_tree,struct averaged_profile** avg_profile);
void output(struct averaged_profile* profile, int iterations, double r_parameter);

void test_init(double** ptr);

// GSL RNG
const gsl_rng_type * T;
gsl_rng * r;
//int seed = -1;
int seed = 153000;


int main()
{
	// The variables
	struct branching_process *tree;
	struct rescaled_profile *rescaled_tree;
	struct averaged_profile *avg_profile;

	// Geometric branching with r = 1 - \sigma / \eps
	// -> pbranch = \sigma = (r-1)/(r-2)
	double r_parameter = 0.5; // Not to be confused with r the GSL-RNG!
	double pbranch = (r_parameter-1)/(r_parameter-2); // pextinction = 1 - pbranch
	int iterations = 1000000;
	initialise(&tree, &rescaled_tree, &avg_profile);
	int iter;	
	for(iter = 0; iter < iterations; iter++)
	{ // Create a tree, rescale it to unit lifetime, add it to statistics
		generate_tree(&tree, pbranch);
		rescale_tree(tree, &rescaled_tree);
		add_to_average(rescaled_tree, &avg_profile);
	}
	output(avg_profile, iterations, r_parameter);
}

void test_init(double** ptr)
{
	*ptr = calloc(10, sizeof(double));
}


void initialise(struct branching_process** init_tree, struct rescaled_profile** rs_tree, struct averaged_profile** avg_profile)
{ // Initialise all structures and RNG
	// Initialise tree
	*init_tree = (struct branching_process* ) malloc(sizeof(struct branching_process));
	(*init_tree)->event_times = malloc((TREEMEMSIZE + 1) * sizeof((*(*init_tree)->event_times)));
	(*init_tree)->population_size = malloc((TREEMEMSIZE + 1) * sizeof(int));
	(*init_tree)->population_size[0] = 1; //prior to t_0 there was nothing
	(*init_tree)->population_size[1] = 1; // prior to t_1 there was one particle
	(*init_tree)->lifetime = 0;
	(*init_tree)->event_counts = 0;
	(*init_tree)->array_max = TREEMEMSIZE;
	assert((*init_tree)->event_times != NULL);
	assert((*init_tree)->population_size != NULL);

	// Initialise rescaled process
	*rs_tree = (struct rescaled_profile*) malloc(sizeof(struct rescaled_profile));
	(*rs_tree)->profile = malloc((RESCALINGRES + 1) * sizeof(int));
	(*rs_tree)->original_lifetime = 0.0;
	(*rs_tree)->weight = 0.0;
	assert((*rs_tree)->profile != NULL);
	
	// Initialise Average profile
	*avg_profile = (struct averaged_profile* ) malloc(sizeof(struct averaged_profile));
	(*avg_profile)->profile = calloc((RESCALINGRES + 1), sizeof(double));
	(*avg_profile)->sum_of_weights = 0.0;
	assert((*avg_profile) != NULL);
	
	/* Initialises GSL Random Generator */
	gsl_rng_env_setup();
  	T = gsl_rng_default;
  	r = gsl_rng_alloc (T);
  	if (seed == -1)	seed = 1000*((int) (time(NULL) % 1000));
  	gsl_rng_set(r, seed);
	printf("#SEED %i\n",seed);

}


void generate_tree(struct branching_process** tree, double pbranch)
{ // generate a tree with given branch probability
	double ttime = 0.0;
	int children;
	int tree_index = 0;
	do
	{
		tree_index++;
		if(tree_index >= (*tree)->array_max){grow(tree);} // Wish for a bigger tree
		ttime += EXP_WAIT((*tree)->population_size[tree_index]);
		(*tree)->event_times[tree_index] = ttime;
		children = offspring(pbranch);
		(*tree)->population_size[tree_index + 1] = ((*tree)->population_size[tree_index] + children); // so pop_size[i] counts size prior to t_i
		if((*tree)->population_size[tree_index + 1] == 0) break;	
	}while(ttime < TIME_MAX); // What if ttime > TIME_MAX? Add to statistics or not?
	(*tree)->event_counts = tree_index;
	(*tree)->lifetime = ttime;
}

void grow(struct branching_process** tree)
{ // enlarges tree structure by ADDTREEMEMSIZE
	*tree = (struct branching_process*) realloc(*tree, ((*tree)->array_max + ADDTREEMEMSIZE + 1) * sizeof(struct branching_process));
	(*tree)->event_times = realloc((*tree)->event_times, (((*tree)->array_max + ADDTREEMEMSIZE)+1) * sizeof(double));
	(*tree)->population_size = realloc((*tree)->population_size, (((*tree)->array_max + ADDTREEMEMSIZE) + 1) * sizeof(int));
	(*tree)->array_max += ADDTREEMEMSIZE;
	assert((*tree) != NULL);
}
int offspring(double pbranch)
{ // Returns number of children, ranging from -1 (extinction) to \infty
	//double rnd = RANDOM_DOUBLE;

	//Dyadic branching
	//return ((rnd < pbranch) ? ((int) 1) :((int) -1));
	
	// Geometric Branching, GSL convention (1 - pbranch) * (pbranch)^(k-1)), k=1,...
	return( (gsl_ran_geometric (r, (1 - pbranch)) - 2) );
	
}

void rescale_tree(struct branching_process* original_tree, struct rescaled_profile** rescaled_tree)
{ // Rescales tree to unit lifetime, computes its weight
	double deltat = (original_tree->lifetime / ((double) RESCALINGRES));
	double next_t;
	int i = 1, j;
	int N = original_tree->event_counts;
	(*rescaled_tree)->original_lifetime = original_tree->lifetime;
	for(j = 0; j < RESCALINGRES; j++)
	{
		next_t = deltat * ((double) j);
		while((i<=N) ? (original_tree->event_times[i] <= next_t + EPS) : (0)) i++;
		(*rescaled_tree)->profile[j] = original_tree->population_size[i];
	}

	(*rescaled_tree)->profile[j] = 1;
	// What is the weight? Discuss this later.
	(*rescaled_tree)->weight = 1.0;
}

void add_to_average(struct rescaled_profile *rs_tree,struct averaged_profile** avg_profile)
{
	int i;
	double inv_weight = (1/(rs_tree->weight));
	for(i = 0; i < (RESCALINGRES + 1); i++)
	{
		(*avg_profile)->profile[i] += (inv_weight * (rs_tree->profile[i]));
	}

	(*avg_profile)->sum_of_weights += (rs_tree->weight);
}

void output(struct averaged_profile* profile, int iterations, double r_parameter)
{
	int i;
	double delta_t = 1/((double) RESCALINGRES);
	double normalisation = 1/(profile->sum_of_weights);

	double integrated_area = 0;
	for(i = 0; i <= RESCALINGRES; i++)
	{
		integrated_area += profile->profile[i];
	}
	double inv_integrated_area = (RESCALINGRES/integrated_area);
	
	printf("#N %i\n", iterations);
	printf("#r %g\n", r_parameter);
	printf("#t/T  n(t/T) \\hat{n}(t/T)\n");
	for(i=0; i < (RESCALINGRES + 1); i++)
	{
		printf("%g\t%g\t%g\n", i*delta_t, normalisation*(profile->profile[i]), inv_integrated_area*(profile->profile[i]));
	}
}
