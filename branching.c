// Branching 0-D, moments
// 04/05/2018

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>

#define RANDOM_DOUBLE gsl_ran_flat(r,0.0,1.0);
#define EXP_WAIT(n) gsl_ran_exponential(r, 1/((double) n))
#define COMPUTE_MOMENTS(p) {long double pp = (long double) p; for(m=0; m <= MAX_MOMENTS; m++){ptcl_moments[next_write_time_index][m] += powl(pp, (long double) m);}}
#define PRINT_MOMENTS {int i,j; for(i=0; i<time_bins; i++){printf("%f\t",write_times[i]);for(j=0; j <= MAX_MOMENTS; j++){printf("%Lf\t",ptcl_moments[i][j]/((long double) iterations));}printf("\n");}}
#define HEADER_MOMENTS printf("#BRANCHINGR %f\n#TIME_MAX %f\n#ITERATIONS %i\n#t\t0th\t1st\t2nd\t3rd\t4th moment\n",branching_r, time_max, iterations);
//#define TIME_BINS 50
//#define TMAX 100.0
#define MAX_MOMENTS 4

long ptcl = 1;
double sigma=-1., eps=-1.,branching_r=-1., time_max=-1., pgeom=0.5;
int iterations=-1,time_bins=-1;
// System Variables
double *write_times;
//long double ptcl_moments[30][MAX_MOMENTS + 1];
long double **ptcl_moments;
double integrated_avalanche_size;

/* GSL Random Number Generator */
const gsl_rng_type * T;
gsl_rng * r;
int seed = -1;

long offspring(void);

/* For Zeta distribution only */
/*double alpha = 1.01; // Power-law exponent, \neq 1
double zeta_alpha;*/
void initialise()
{
	/* Initialises GSL Random Generator */
	gsl_rng_env_setup();
  	T = gsl_rng_default;
  	r = gsl_rng_alloc (T);
  	if (seed == -1)	seed = 100*((int) (time(NULL) % 1000));
  	gsl_rng_set(r, seed);

	/* Zeta distribution only */

	//zeta_alpha = gsl_sf_zeta(alpha);
	
	/* Init system variables */
	if(branching_r==-1){branching_r = 0.1;}
	//else {eps=1/(1.0 + branching_r); sigma = branching_r*eps;} // branching_r = \sigma/\eps, s.t. \sigma+\eps=1
	if(iterations==-1)iterations = 1000;
	if(time_bins == -1) time_bins = 10;
	if(time_max == -1){time_max = 1000.0;}

	/* Init moments */
	int i,j;

	ptcl_moments = malloc(time_bins * sizeof(long double *));
	for(i = 0; i < time_bins; i++)
	{
		ptcl_moments[i] = malloc( (MAX_MOMENTS + 1) * sizeof(long double));
	}

	write_times = malloc(time_bins * sizeof(double));
	for(i=0;i<time_bins;i+=1)
	{
		write_times[i] = pow(time_max,(((double)i+1.)/( (double) time_bins)));
		//printf("Write time[%i] = %g\n",i,write_times[i]);
		for(j=0; j <= MAX_MOMENTS; j++)
		{
			ptcl_moments[i][j] = 0;
		}
	}

	
}

void printhelp(void);

void printhelp()
{
	printf("Branching simulation. Parameters:\n \
			-r Critical parameter (geometric probability for offspring, p=1/(2-r) )\n \
			-B how many time points do you want to have?\n \
			-T What is TMAX, until when do I have to keep up?\n \
			-I How many iterations?\n ");
}

int main(int argc, char *argv[])
{
	/* getopt: sigma, eps, iterations, time_bins, time_max, seed */
	opterr = 0;
	int c = 0;
	while( (c = getopt (argc, argv, "r:T:I:B:S:h") ) != -1)
		switch(c)
			{
				case 'r':
					branching_r = atof(optarg);
					break;
				case 'I':
					iterations = atoi(optarg);
					break;
				case 'B':
					time_bins = atoi(optarg);
					break;
				case 'T':
					time_max = atof(optarg);
					break;
				case 'S':
					seed = atoi(optarg);
					break;
				case 'h':
					printhelp();
					return 0;
			default:
				exit(EXIT_FAILURE);
			}




	initialise();

	double rnd, pbranch, ttime;
	int iter, next_write_time_index=0, m;

	// pbranch = 0.5*(1-branching_r);
	// pgeom = 1/(2-branching_r);
	pgeom = ((branching_r - 1)/(branching_r - 2));

	for(iter=0;iter < iterations; iter+=1)
	{
		ptcl=1;
		integrated_avalanche_size = 0.0;
		ttime=0;
		next_write_time_index = 0;
		do
		{
			ttime += EXP_WAIT(ptcl);
			//rnd = RANDOM_DOUBLE;
			//Write out if time surpasses writing time
			while(ttime > write_times[next_write_time_index])
			{
				//printf("#DBG ttime %f, next_write_time_index %i, write_time %f, ptcl %ld\n",ttime, next_write_time_index, write_times[next_write_time_index], ptcl);
				COMPUTE_MOMENTS(ptcl);
				next_write_time_index += 1;
				if(next_write_time_index==time_bins) break;
			}
			//
			//if(rnd < pbranch) ptcl += 1;
			//else ptcl-=1;
			integrated_avalanche_size += (ttime * ((double) ptcl)); 

			ptcl += offspring();
			if(ptcl == 0) break;
		}while(next_write_time_index < time_bins);

		printf("#IAS %f\n", integrated_avalanche_size);
	}

	HEADER_MOMENTS;
	PRINT_MOMENTS;
	return 0;
}

long offspring(void)
{
/* Geometric distribution*/
	long k = ((long) gsl_ran_geometric (r, pgeom) - 2);
	return k;
 
	/* Zeta distribution */
	/*double x = gsl_ran_flat(r,0.0,1.0);
	
	long k;
	k = ((long) floor(pow(x*zeta_alpha,-1/alpha)) - 1);
	return k;*/
	}
