// Branching 0-D, moments
// 01/07/2018

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <assert.h> // Assert
#include <time.h>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>

#define RANDOM_DOUBLE gsl_ran_flat(r,0.0,1.0);
#define EXP_WAIT(n) gsl_ran_exponential(r, 1/((double) n))
#define IJ2L(i,j) (i*time_bins_c - i*(i-1)/2 + (j-i))

#define COMPUTE_MOMENTS(p) {long double pp = (long double) p; for(m=0; m <= MAX_MOMENTS; m++){ptcl_moments[next_write_time_index][m] += powl(pp, (long double) m);}}

#define PRINT_MOMENTS {for(i=0; i<time_bins; i++){printf("%f\t",write_times[i]);for(j=0; j <= MAX_MOMENTS; j++){printf("%Lf\t",ptcl_moments[i][j]/((long double) iterations));}printf("\n");}}

#define HEADER_MOMENTS printf("#BRANCHINGR %f\n#TIME_MAX %f\n#ITERATIONS %i\n#t\t0th\t1st\t2nd\t3rd\t4th moment\n",branching_r, time_max, iterations);
//#define TIME_BINS 50
//#define TMAX 100.0

#define HEADER_CORRELATIONS {printf("#TIME TIME CORRELATIONS\n#BRANCHINGR %f\n#TIME_MAX %f\n#ITERATIONS %i\n",branching_r, time_max, iterations);	printf("#time");for( i = 0; i < time_bins_c; i++){printf("\t%g", corr_measure_times[i]);}printf("\n");}


#define MAX_MOMENTS 4

long ptcl = 1;
double sigma=-1., eps=-1.,branching_r=-1, time_max=-1., eps_drift=0.0, eps_diff=0.0;
int iterations=-1,time_bins=-1,time_bins_c=-1;
// System Variables
double *write_times; // At what times shall moments be written out?
double *corr_measure_times; // At what times shall correlations be written out?
long double **ptcl_moments; // Moment of living particles, written out at write_times;
long double *tt_corr; // length 0.5*CORR_TIMES*(CORR_TIMES-1) average of all the buffers.
double *tt_corr_buffer; // Store here for each realisation
long double *tt_corr_avg; // I want to take moments for different times than those...I want to take moments for. As i want exponential spacing for moment calculation, and linear spacing for correlations, this array will store averages for the latter not the former.

/* GSL Random Number Generator */
const gsl_rng_type * T;
gsl_rng * r;
int seed = -1;

// Function declaration
double noisy_ext_rate(double, double, double);
long offspring(double);
void clear_buffer(double*);
void printhelp(void);
void initialise(void);

/* For Zeta distribution only */
/*double alpha = 1.01; // Power-law exponent, \neq 1
double zeta_alpha;*/

int main(int argc, char *argv[])
{
	/* getopt: sigma, eps, iterations, time_bins, time_max, seed */
	opterr = 0;
	int c = 0;
	while( (c = getopt (argc, argv, "r:D:u:T:I:B:C:S:h") ) != -1)
		switch(c)
			{
				case 'r':
					branching_r = atof(optarg);
					break;
				case 'D':
					eps_diff = atof(optarg);
					break;
				case 'u':
					if (atof(optarg) > 0){eps_drift = (1/atof(optarg));}
					else {printf("-u has to be positive real. BREAK\n"); return 0;}
					break;
				case 'I':
					iterations = atoi(optarg);
					break;
				case 'B':
					time_bins = atoi(optarg);
					break;
				case 'C':
					time_bins_c = atoi(optarg);
					break;
				case 'T':
					time_max = atof(optarg);
					break;
				case 'S':
					seed = atoi(optarg);
					break;
				case 'h':
					printhelp();
					return 0;
			default:
				exit(EXIT_FAILURE);
			}




	initialise();

	double /*rnd,  pbranch,*/ ttime, p_geom_avg, pext, delta_t = 0.0;
	int iter, next_write_time_index=0, next_corr_write_time_index=0,i,j, m, neg_ptcl_counter = 0;

	p_geom_avg = ((branching_r - 1) /(branching_r - 2));

	/* Simulate Ensemble */
	for(iter=0;iter < iterations; iter+=1)
	{
		ptcl=1;
		ttime=0;
		next_write_time_index = 0;
		next_corr_write_time_index = 0;
		clear_buffer(tt_corr_buffer); // Overwrite with zeros

		do
		{
			if(0) printf("iter %i, ttime: %g, delta_t: %g, n_c_w_t_i %i, corr_measure_times %g, n_w_t_i %i, write_times %g, pext %g, p_geom_avg %g, ptcl: %li\n",iter, ttime, delta_t, next_corr_write_time_index, corr_measure_times[next_corr_write_time_index], next_write_time_index, write_times[next_write_time_index], pext, p_geom_avg, ptcl);
			delta_t = EXP_WAIT(ptcl);
			ttime += delta_t;
			while(ttime > corr_measure_times[next_corr_write_time_index]) // This only works when both corr and write_times end at same time.
			{
				tt_corr_buffer[next_corr_write_time_index] = ( (double) ptcl);
				next_corr_write_time_index += 1;
				if(next_corr_write_time_index == time_bins_c) break;
			}
			while(ttime > write_times[next_write_time_index]) // Same for averages
			{
				COMPUTE_MOMENTS(ptcl);
				next_write_time_index += 1;
				if(next_write_time_index == time_bins) break;
			}
			pext = noisy_ext_rate(p_geom_avg, pext, delta_t);
			if( pext < 0 || pext > 1)
			{
				// Throw away entire thing and restart if particle density becomes negative. 
				// Resulting observables are conditioned on reasonable extinction probability
				neg_ptcl_counter++;
				ptcl=1;
				ttime=0;
				next_write_time_index = 0;
				next_corr_write_time_index = 0;
				clear_buffer(tt_corr_buffer); // Overwrite with zeros
			}
			ptcl += offspring(pext);
			if(ptcl == 0) break;
		}while(next_corr_write_time_index < time_bins_c);
		
		for(i=0; i < time_bins_c; i++)
		{
			tt_corr_avg[i] += ( (long double) (tt_corr_buffer[i]));
		}

		/* Commit Correlations */ // HERE THE SPURIOUS ERROR OCCURS
		for(i = 0; i < time_bins_c; i++){
			for(j=i; j < time_bins_c; j++)
			{
				m = IJ2L(i,j);
				tt_corr[m] += ( (long double) (tt_corr_buffer[i] * tt_corr_buffer[j]) );
			}
		}
	} 
	HEADER_MOMENTS;
	PRINT_MOMENTS;
	// HEADER_CORRELATIONS;
//
//	long double inv_iterations = 1/((long double) iterations );
//	
//	/* Normalise all the moments */
//	for(i = 0; i < time_bins_c; i++)
//	{
//		tt_corr_avg[i] = inv_iterations * tt_corr_avg[i];
//	}
//	
//	/* Compute Correlations */
//	// This block somehow changes tt_corr_avg.
//	for(i=0; i<time_bins_c; i++)
//	{
//		for(j=i; j <time_bins_c; j++)
//		{	
//			m = IJ2L(i,j);
//			tt_corr[m] = (inv_iterations*tt_corr[m]);
//		}
//	}
//	
//	/* Compute Connected Correlations */
//	for(i=0; i<time_bins_c; i++)
//	{
//		for(j=i; j <time_bins_c; j++)
//		{	
//			m = IJ2L(i,j);
//			tt_corr[m] = (tt_corr[m] - (tt_corr_avg[i]*tt_corr_avg[j]));
//		}
//	}
//	
//	/* Print out Connected Correlations (for julia matrixplot) */
//	/* This would spit out a NxN array, not ideal for pgfplots */
//	for(i=0; i<time_bins_c; i++)
//	{
//		printf("%g\t",corr_measure_times[i]);
//		for(j=0; j < i; j++)
//		{
//			m = IJ2L(j,i); // reverse order for lower triangular part
//			//printf("(%i,%i):%i ",i,j,m);
//			printf("%Lf\t",tt_corr[m]);
//		}
//		
//		for(j=i; j < time_bins_c; j++)
//		{
//			m = IJ2L(i,j);
//			//printf("(%i,%i):%i ",i,j,m);
//			printf("%Lf\t",tt_corr[m]);
//		}
//		printf("\n");
//	}
//	

	/* Print out Connected Correlation (for pgfplots) */
	// (x,y,z)
	/*for(i = 0; i < time_bins_c; i++)
	{
		for(j = 0; j < i; j++)
		{
			m = IJ2L(j,i); // Simply copied from above
			printf("%f\t%f\t%Lf\n", corr_measure_times[i], corr_measure_times[j], tt_corr[m]);
		}
		for(j = i; j < time_bins_c; j++)
		{
			m = IJ2L(i,j);
			printf("%f\t%f\t%Lf\n", corr_measure_times[i], corr_measure_times[j], tt_corr[m]);
		}	
	}
	*/
	
	// How many runs had to be thrown away?
	printf("# %i out of %i runs discarded (%g %%)\n", neg_ptcl_counter, (neg_ptcl_counter + iterations), 100*(((double) neg_ptcl_counter )/ ((double) (iterations + neg_ptcl_counter))));

	return 0;
}

long offspring(double pext)
{
/* This is offspring distribution. Note that output range is [-1,\infty], p_{-1} being extinction probability */
	/* Geometric distribution (pgeom=pextinction)*/
	
	long k = ((long) gsl_ran_geometric (r, pext) - 2);
	return k;
 
	/* Zeta distribution */
	/*double x = gsl_ran_flat(r,0.0,1.0);
	
	long k;
	k = ((long) floor(pow(x*zeta_alpha,-1/alpha)) - 1);
	return k;*/
}

double noisy_ext_rate(double p_geom_avg,double old_pext, double delta_t)
{
	if(eps_diff == 0 || eps_drift == 0) {return old_pext;}
	else{
	double propagator_D = (eps_diff*(1-exp(-2*eps_drift*delta_t))/eps_drift );
	double propagator_mu = (p_geom_avg - (old_pext - p_geom_avg)*exp(-eps_drift*delta_t));
	double pext_new = (gsl_ran_gaussian_ziggurat(r, propagator_D) + propagator_mu);
	return pext_new;}
}


void printhelp()
{
	printf("Branching simulation with noisy extinction\n \
		Parameters:\n \
		-r Critical parameter (geometric probability for offspring, p_branch=1/(2-r) )\n \
		-D diffusion constant of epsilon fluctuations \n \
		-u timescale of relaxation for epsilon fluctuations\n \
		-B how many time points do you want to have?\n \
		-T What is TMAX, until when do I have to keep up?\n \
		-I How many iterations?\n ");
}

void initialise()
{
	/* Initialises GSL Random Generator */
	gsl_rng_env_setup();
  	T = gsl_rng_default;
  	r = gsl_rng_alloc (T);
  	if (seed == -1)	seed = 100*((int) (time(NULL) % 1000));
  	gsl_rng_set(r, seed);

	/* Zeta distribution only */

	//zeta_alpha = gsl_sf_zeta(alpha);
	
	/* Init system variables */
	if(branching_r==-1){branching_r = 0.1;}
	if(iterations==-1)iterations = 1000;
	if(time_bins == -1) time_bins = 10;
	if(time_bins_c == -1) time_bins_c = 10;
	if(time_max == -1){time_max = 100.0;}

	/* Init moments */
	int i,j;

	ptcl_moments = malloc(time_bins * sizeof(long double *));
	assert(ptcl_moments != NULL);
	for(i = 0; i < time_bins; i++)
	{
		ptcl_moments[i] = malloc( (MAX_MOMENTS + 1) * sizeof(long double));
	}

	write_times = malloc(time_bins * sizeof(double));
	assert(write_times != NULL);
	for(i=0;i<time_bins;i+=1)
	{
		write_times[i] = pow(time_max,(((double)i+1.)/( (double) time_bins)));
		for(j=0; j <= MAX_MOMENTS; j++)
		{
			ptcl_moments[i][j] = 0;
		}
	}

	corr_measure_times = malloc(time_bins_c * sizeof(double));
	assert(corr_measure_times != NULL);
	for(i=0; i<time_bins_c; i++)
	{
		corr_measure_times[i] = (((double) (i)) * (time_max/time_bins_c)); //linear spaced
	}

	tt_corr_buffer = malloc(time_bins_c * sizeof(double));
	assert(tt_corr_buffer != NULL);
	int len_tt_corr = ((time_bins_c - 1)*time_bins_c - (time_bins_c - 1)*(time_bins_c-2)/2 + 1); // Independent entries symmetric matrix
	printf("#LEN_TT_CORR %i\n",len_tt_corr);
	tt_corr = calloc(len_tt_corr, sizeof(long double));	
	assert(tt_corr != NULL);	
	tt_corr_avg = calloc(time_bins_c, sizeof(long double) );
	assert(tt_corr_avg != NULL);
}

void clear_buffer(double *ptr)
{
	/* Clears buffer of trajectory buffer before running new simulation*/
	int i=0;
	for( ; i < time_bins_c; i++)
	{
		ptr[i] = 0.;
	}
}
